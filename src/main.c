/*
    Copyright (C) 2016 Kerttu Liis Lootus

    This file is part of my-course-repository.

    my-course-repository is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    my-course-repository is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with my-course-repository.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <stdio.h>
#include <string.h>
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "rfid.h"
#include "hmi_msg.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "uart-wrapper.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/helius_microrl/microrl.h"
#include "cli_microrl.h"


#define BLINK_DELAY_MS 100
#define BAUD 9600


volatile uint32_t time;


int in_list;


int done;


microrl_t rl;
microrl_t *prl = &rl;


static inline void init_counter(void)
{
    time = 0;
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12);
    TCCR1B |= _BV(CS12);
    OCR1A = 62549;
    TIMSK1 |= _BV(OCIE1A);
}


static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}


static inline void init_io(void)
{
    DDRA |= _BV(DDA3);
    DDRA |= _BV(DDA1);
    uart3_init(UART_BAUD_SELECT(BAUD, F_CPU));
    uart0_init(UART_BAUD_SELECT(BAUD, F_CPU));
    stderr = &uart3_out;
    stdin = stdout = &uart0_io;
    sei();
}


static inline void init_microrl(void)
{
    microrl_init(prl, print);
    microrl_set_execute_callback (prl, cli_execute);
}


static inline void print_info(void)
{
    fprintf_P(stderr, PSTR(PRO1 GIT_DESCR PRO2 __DATE__ " " __TIME__ "\n" ));
    fprintf_P(stderr, PSTR(LIBC __AVR_LIBC_VERSION_STRING__ " " AVR __VERSION__
                           "\n"));
    fprintf_P(stdout, PSTR(STUD_NAME "\n"));
}


static inline void heartbeat(void)
{
    static uint32_t last_time;
    uint32_t current_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        current_time = time;
    }

    if ((last_time - current_time) > 0) {
        fprintf_P(stderr, PSTR(UPTIME "\n"), time);
        PORTA ^= _BV(PORTA3);
    }

    last_time = current_time;
}



void check(void)
{
    static uint32_t since_allowed;
    static uint32_t since_blocked;

    if (PICC_IsNewCardPresent()) {
        int found = 0;

        if (head != NULL) {
            card_t *current;
            current = head;

            while (current != NULL) {
                Uid card_uid;
                Uid *uid_ptra = &card_uid;
                PICC_ReadCardSerial(uid_ptra);

                if (!memcmp(card_uid.uidByte, current->uid, card_uid.size)) {
                    in_list = 1;
                    PORTA |= _BV(PORTA1);
                    since_allowed = time;
                    lcd_clrscr();
                    lcd_puts_P(PSTR(STUD_NAME));
                    lcd_goto(0x40);
                    lcd_puts(current->name);
                    done = 0;
                    found = 1;
                }

                current = current->next;
            }
        }

        if (head == NULL || found == 0) {
            in_list = 0;
            PORTA &= ~_BV(PORTA1);
            since_blocked = time;
            lcd_clrscr();
            lcd_puts_P(PSTR(STUD_NAME));
            lcd_goto(0x40);
            lcd_puts_P(PSTR(ACCESS));
            done = 0;
        }

        in_list = found;
    }

    if (in_list == 1 && (time - since_allowed) < 3) {
        PORTA |= _BV(PORTA1);
    } else {
        PORTA &= ~_BV(PORTA1);
    }

    if (((time - since_blocked) >= 7) && ((time - since_allowed) >= 7) &&
            done == 0) {
        lcd_clrscr();
        lcd_puts_P(PSTR(STUD_NAME));
        done = 1;
    }
}


int main (void)
{
    init_rfid_reader();
    init_io();
    init_counter();
    print_info();
    init_microrl();
    lcd_init();
    lcd_clrscr();
    lcd_puts_P(PSTR(STUD_NAME));
    done = 1;

    while (1) {
        heartbeat();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
        check();
    }
}


ISR(TIMER1_COMPA_vect)
{
    time++;
}
